
const music=document.querySelector("audio");
const play=document.getElementById("play");
const image=document.querySelector("img");
const title=document.getElementById("title");
const prev=document.getElementById("previous");
const next=document.getElementById("next");

const songs=[
    {
    name:"baby",
    title:"Baby",
    artist:"Justin Bieber", 
    },
    {
        name:"LetMeLoveYou",
        title:"Let Me Love You",
        artist:"Justin Bieber", 
    },
    {
        name:"alone",
        title:"Alone",
        artist:"Alan Walker", 
    },
    {
        name:"OnMyWay",
        title:"On My Way",
        artist:"Alan Walker", 
    }
];

let isPlaying=false;

const playMusic= () => {
    isPlaying=true;
    music.play();
    image.classList.remove("stop");
    play.classList.replace("fa-play","fa-pause");
    image.classList.add("animate");
};

const pauseMusic= () => {
    isPlaying=false;
    image.classList.add("stop");
    music.pause();
    play.classList.replace("fa-pause","fa-play");
    image.classList.add("animate");
};

play.addEventListener('click',()=>{
    if(isPlaying){
        pauseMusic();
    }
    else{
        playMusic();
    }
});

const loadSongs= (songs)=>{
    title.textContent=songs.title;
    artist.textContent=songs.artist;
    music.src= "audios/"+ songs.name + ".mp3";
    image.src="images/"+ songs.name + ".jpg";
};


songsIndex=2;

const nextSong=()=>{
    songsIndex=(songsIndex -1 + songs.length) % songs.length;
    pauseMusic();
    
    loadSongs(songs[songsIndex]);
    playMusic();
}

const previousSong=()=>{
    songsIndex=(songsIndex-1)%songs.length;
    loadSongs(songs[songsIndex]);
}

next.addEventListener('click',nextSong);
prev.addEventListener('click',previousSong);

loadSongs(songs[3]);
